<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Ramsey\Uuid\Type\Integer;

class BarangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('barangs')->insert([
            'nama_barang' =>  'Kecap Bango',
            'harga_jual' => 5000,
            'harga_beli' => 4500,
            'stok_barang' => 80,
            'cover' => 'no-image.png',
            'updated_at' => Carbon::now(),
            'created_at' => Carbon::now(),
        ]);
        DB::table('barangs')->insert([
            'nama_barang' =>  'Nescafe Kotak',
            'harga_jual' => 8000,
            'harga_beli' => 6000,
            'stok_barang' => 20,
            'cover' => 'no-image.png',
            
            'updated_at' => Carbon::now(),
            'created_at' => Carbon::now(),
        ]);
        DB::table('barangs')->insert([
            'nama_barang' =>  'Aqua Botol',
            'harga_jual' => 3500,
            'harga_beli' => 3000,
            'stok_barang' => 100,
            
            'cover' => 'no-image.png',
            'updated_at' => Carbon::now(),
            'created_at' => Carbon::now(),
        ]);
        DB::table('barangs')->insert([
            'nama_barang' =>  'Nivea Handbody',
            'harga_jual' => 12000,
            'harga_beli' => 10000,
            'stok_barang' => 20,
            'cover' => 'no-image.png',
            'updated_at' => Carbon::now(),
            'created_at' => Carbon::now(),
        ]);
    }
}
