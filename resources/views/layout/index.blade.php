<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="{{ secure_asset('css/app.css') }}" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <title>Nutech CRUD</title>
</head>

<body class="mx-auto max-w-7xl bg-gray-100">
    @yield('content')

    {{-- @include('sweetalert::alert') --}}
</body>

</html>
@stack('scripts')
