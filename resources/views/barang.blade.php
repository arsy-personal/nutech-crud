{{-- @@extends('name') --}}
@extends('layout.index')

@section('content')
<div class="w-full  min-h-screen flex flex-col">
    {{-- Header --}}
    <div class="flex flex-row justify-center h-20 w-full align-middle ">
        <div class="flex-1 items-center flex">
            <h1 class="text-gray-700 font-bold text-3xl">Daftar Barang ({{count($barangs)}}) </h1>
        </div>
        <div class="flex-1 items-center justify-end flex">
            <button class="p-4 bg-blue-600 text-white rounded-lg" onclick="tambahBarang()">+ Tambah Barang</button>
        </div>
    </div>
    {{-- Barang --}}
    <div class="flex bg-gray-200 flex-wrap flex-row ">
        {{-- Product Card --}}
        @foreach ($barangs as $item)
        <div class="w-4/12  px-2 py-2 flex-none flex-shrink-0	">
            <div class="w-full h-full bg-white rounded-lg flex flex-row ">
                <div class="w-4/12 h-full bg-gray-300">
                    <img class="object-cover w-full h-full" src="images/no-image.jpg" />
                    {{-- <img class="object-cover w-full h-full" src="storage/cover/{{$item->cover}}" /> --}}
                    {{-- <img class="object-cover w-full h-full" src="{{ asset('storage/cover/')}}/{{$item->cover}}" />
                    --}}
                </div>
                <div class="p-4 flex flex-row flex-wrap flex-1 gap-y-2">
                    <h4 class="text-lg font-bold text-gray-700 flex-grow min-w-full ">{{$item->nama_barang}}
                    </h4>
                    <div class="w-6/12">
                        <span>Harga Jual</span>
                        <p>{{$item->harga_jual}}</p>
                    </div>
                    <div class="w-6/12">
                        <span>Harga Beli</span>
                        <p>{{$item->harga_beli}}</p>
                    </div>
                    <div class="min-w-full ">
                        <span>Stock</span>
                        <p>{{$item->stok_barang}}</p>
                    </div>
                    <div class="min-w-full ">
                        <button class="bg-red-600 p-2 text-white rounded-lg" id="btn-delete"
                            data-href=" {{ route('barangs.destroy', $item->id) }} "
                            onclick="modalHandler('{{ route('barangs.destroy', $item->id) }}')">
                            Hapus
                        </button>
                        <button class="bg-green-600 p-2 text-white rounded-lg" id="btn-delete"
                            {{-- data-href=" {{ route('barangs.destroy', $item->id) }} " --}}
                            onclick=" editBarang({{ $item->id }})">
                            Edit
                        </button>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    {{$barangs->links()}}

    <div id="modal-add"
        class="hidden fixed flex-col z-10 inset-0 overflow-y-auto w-3/5 bg-white my-auto mx-auto h-4/5 rounded-lg shadow-xl ">
        <div class="flex flex-row justify-between bg-gray-50 p-4">
            <p>Tambah Barang</p>
            <button onclick="tambahBarang()">Close</button>
        </div>
        <div class="flex flex-1 p-4">
            <form class="w-full h-full" enctype="multipart/form-data" method="POST"
                action=" {{ route('barangs.store') }} ">
                @csrf
                <div class="flex flex-col">
                    <label for="nama_barang">Nama Barang</label>
                    <input class="border p-2 rounded-lg" type="text" id="nama_barang" name="nama_barang"
                        placeholder="Nama Barang">
                </div>
                <div class="flex flex-row flex-wrap my-4">
                    <div class="flex flex-col">
                        <label for="harga_jual">Harga Jual</label>
                        <input class="border p-2 rounded-lg" type="number" id="harga_jual" name="harga_jual"
                            placeholder=0>
                    </div>
                    <div class="flex flex-col">
                        <label for="harga_beli">Harga beli</label>
                        <input class="border p-2 rounded-lg" type="number" id="harga_beli" name="harga_beli"
                            placeholder=0>
                    </div>
                    <div class="flex flex-col">
                        <label for="stok_barang">stok stok</label>
                        <input class="border p-2 rounded-lg" type="number" id="stok_barang" name="stok_barang"
                            placeholder=0>
                    </div>

                </div>
                <div class="flex flex-col my-4">
                    <input type="file" name="cover" id="cover">
                </div>
                {{-- <button>close</button> --}}
                <button class="p-4 bg-blue-500 text-white rounded-lg" type="submit">simpan</button>
            </form>
        </div>
    </div>
    <div id="modal-edit"
        class="hidden fixed flex-col z-10 inset-0 overflow-y-auto w-3/5 bg-white my-auto mx-auto h-4/5 rounded-lg shadow-xl ">
        <div class="flex flex-row justify-between bg-gray-50 p-4">
            <p>Edit Barang</p>
            <button onclick="editBarang()">Close</button>
        </div>
        <div class="flex flex-1 p-4">
            <form action="" id="form_edit" class="w-full h-full" enctype="multipart/form-data" method="POST">
                @method('PUT')
                @csrf
                <div class=" flex flex-col">
                    <label for="edit_nama_barang">Nama Barang</label>
                    <input class="border p-2 rounded-lg" type="text" id="edit_nama_barang" name="edit_nama_barang"
                        placeholder="Nama Barang">
                </div>
                <div class="flex flex-row flex-wrap my-4">
                    <div class="flex flex-col">
                        <label for="edit_harga_jual">Harga Jual</label>
                        <input class="border p-2 rounded-lg" type="number" id="edit_harga_jual" name="edit_harga_jual"
                            placeholder=0>
                    </div>
                    <div class="flex flex-col">
                        <label for="edit_harga_beli">Harga beli</label>
                        <input class="border p-2 rounded-lg" type="number" id="edit_harga_beli" name="edit_harga_beli"
                            placeholder=0>
                    </div>
                    <div class="flex flex-col">
                        <label for="edit_stok_barang">stok stok</label>
                        <input class="border p-2 rounded-lg" type="number" id="edit_stok_barang" name="edit_stok_barang"
                            placeholder=0>
                    </div>

                </div>
                <div class="flex flex-col my-4">
                    <input type="file" name="edit_cover" id="edit_cover">
                </div>
                {{-- <button>close</button> --}}
                <button class="p-4 bg-blue-500 text-white rounded-lg" type="submit">simpan</button>
            </form>
        </div>
    </div>


    <div id="modal-delete" class="fixed z-10 inset-0 overflow-y-auto hidden " aria-labelledby="modal-title"
        role="dialog" aria-modal="true">
        <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
            <div class="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" aria-hidden="true"></div>
            <span class="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span>
            <div
                class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full">
                <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                    <div class="sm:flex sm:items-start">
                        <div
                            class="mx-auto flex-shrink-0 flex items-center justify-center h-12 w-12 rounded-full bg-red-100 sm:mx-0 sm:h-10 sm:w-10">
                            <svg class="h-6 w-6 text-red-600" xmlns="http://www.w3.org/2000/svg" fill="none"
                                viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                    d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z" />
                            </svg>
                        </div>
                        <div class="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
                            <h3 class="text-lg leading-6 font-medium text-gray-900" id="modal-title">
                                Delete
                            </h3>
                            <div class="mt-2">
                                <p class="text-sm text-gray-500">
                                    Apakah anda yakin akan menghapus data ini ?
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                    <form id="form_delete" method="POST" action="">
                        @method('DELETE')
                        @csrf
                        <button id="btn-delete-modal"
                            class="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-red-600 text-base font-medium text-white hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500 sm:ml-3 sm:w-auto sm:text-sm">
                            Hapus
                        </button>
                    </form>
                    <button type="button" onclick="modalHandler()"
                        class="mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm">
                        Batal
                    </button>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
@push('scripts')
<script>
    function modalHandler(id) {
        const modal = document.getElementById('modal-delete')
        const route = id
        if (modal.classList.contains('hidden')) {
            modal.classList.remove('hidden')
            // alert(route)
            document.getElementById('form_delete').setAttribute('action', route)
            // if
            document.getElementById('btn-delete-modal').onclick(() => {
                document.forms["form_delete"].submit();
                // jqsus
            })
        } else {
            modal.classList.add('hidden')
            document.getElementById('form_delete').setAttribute('action', '')
        }
    }

    function editBarang(id) {
        const modal = document.getElementById('modal-edit')
        const route = "{{route('barangs.update', 'id')}}"

        if (modal.classList.contains('hidden')) {
            modal.classList.remove('hidden')
            document.getElementById('form_edit').setAttribute('action', route.replace('id', id))

            $.ajax({
                type: "get",
                url: "barangs/" + id + "/edit",
                dataType: "json",
                success: function (response) {

                    $('#edit_nama_barang').val(response.nama_barang);
                    $('#edit_harga_jual').val(response.harga_jual);
                    $('#edit_harga_beli').val(response.harga_beli);
                    $('#edit_stok_barang').val(response.stok_barang);
                    $('#edit_cover').val(response.cover);

                }
            });

        } else {
            modal.classList.add('hidden')
        }
    }

    function tambahBarang() {
        const modal = document.getElementById('modal-add')
        if (modal.classList.contains('hidden')) {
            modal.classList.remove('hidden')
        } else {
            modal.classList.add('hidden')
        }
    }

</script>
@endpush
