<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use Facade\FlareClient\View;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $dataBarang = Barang::paginate(6);
        return View('barang')->with('barangs', $dataBarang);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_barang' => 'required',
            'harga_jual' => 'required',
            'harga_beli' => 'required',
            'stok_barang' => 'required',
            'cover' => 'image|nullable|max:1999'
        ]);

        if ($request->hasFile('cover')) {
            # code...
            $fileNameWithExt = $request->file('cover')->getClientOriginalName();
            $filname = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('cover')->getClientOriginalExtension();
            $fileNameToStore = $filname.'_'.time().'.'.$extension;
            $path = $request->file('cover')->storeAs('/public/cover', $fileNameToStore);
        } else {
            $fileNameToStore = 'no-image.png';
        };
        //
        $barang = new Barang;
        $barang->nama_barang = $request->nama_barang;
        $barang->harga_jual = $request->harga_jual;
        $barang->harga_beli = $request->harga_beli;
        $barang->stok_barang = $request->stok_barang;
        $barang->cover = $fileNameToStore;
        $barang->save();
        // $barang->foto = $request->foto;
        // $barang->
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function show(Barang $barang)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $barang = Barang::find($id);
        echo json_encode($barang);
        exit;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Barang  $barang
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'edit_nama_barang' => 'required',
            'edit_harga_jual' => 'required',
            'edit_harga_beli' => 'required',
            'edit_stok_barang' => 'required',
            'edit_cover' => 'image|nullable|max:1999'
        ]);
        if ($request->hasFile('edit_cover')) {
            # code...
            $fileNameWithExt = $request->file('edit_cover')->getClientOriginalName();
            $filname = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('edit_cover')->getClientOriginalExtension();
            $fileNameToStore = $filname.'_'.time().'.'.$extension;
            $path = $request->file('edit_cover')->storeAs('/public/cover', $fileNameToStore);
        }; 
        $barang = Barang::find($id);
        $barang->nama_barang = $request->edit_nama_barang;
        $barang->harga_jual = $request->edit_harga_jual;
        $barang->harga_beli = $request->edit_harga_beli;
        $barang->stok_barang = $request->edit_stok_barang;
        if ($request->hasFile('edit_cover')){
            $barang->cover = $fileNameToStore;
        } 
        $barang->save();
        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Barang  $barang
     * @return \Illuminate\Http\Response
     */
 

    public function destroy($id)
    {
        $barang = Barang::destroy($id);
        return redirect('/');

    }
}
