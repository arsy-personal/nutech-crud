<?php

use App\Http\Controllers\BarangController;
use Illuminate\Support\Facades\Route;
// use 

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::resource('/', BarangController::class);
Route::resource('barangs', BarangController::class);
// Route::get('/barangs/{id}', [BarangController::class, 'delete'])->name('delete');
Route::get('barangs/delete/{id}', [BarangController::class, 'delete'])->name('barangs.delete');

// Route::get('/barangs')
// Route::delete('/barangs/{barang}', BarangController::class);